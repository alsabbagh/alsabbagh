package com.techConcept.alsabbagh_institute;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.techConcept.alsabbagh_institute.adapters.CourseAdapter;
import com.techConcept.alsabbagh_institute.models.Section;
import com.techConcept.alsabbagh_institute.server.APIs;
import com.techConcept.alsabbagh_institute.server.ServerManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import techContept.theinstitute.R;

public class AddSectionActivity extends AppCompatActivity {


    RecyclerView sectionsRV;
    ArrayList<Section> sections;
    LinearLayout progress;
    BasicData basicData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_section);

        basicData = BasicData.getInstance(this);
        progress = findViewById(R.id.progressLL);
        sectionsRV = findViewById(R.id.sectionsRV);
        sectionsRV.setLayoutManager(new LinearLayoutManager(AddSectionActivity.this,
                LinearLayoutManager.VERTICAL, false));

        getSections();
    }

    private void getSections() {
        sections = new ArrayList<>();

        progress.setVisibility(View.VISIBLE);
        ServerManager.get(AddSectionActivity.this, APIs.allSections,
                basicData.getTOKEN(), progress, result -> {
                    try {
                        JSONArray data = new JSONArray(new JSONObject(result)
                                .getString("data"));
                        for (int i = 0; i < data.length(); i++) {
                            sections.add(new Section(
                                    data.getJSONObject(i).getString("id"),
                                    data.getJSONObject(i).getString("title"),
                                    data.getJSONObject(i).getString("description")));

                        }
                        progress.setVisibility(View.GONE);
                        sectionsRV.setAdapter(new CourseAdapter(AddSectionActivity.this, sections, true));
                    } catch (JSONException e) {
                        progress.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                });
    }
}