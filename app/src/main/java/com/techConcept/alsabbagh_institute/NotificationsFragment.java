package com.techConcept.alsabbagh_institute;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.techConcept.alsabbagh_institute.adapters.NotificationAdapter;
import com.techConcept.alsabbagh_institute.models.Notification;
import com.techConcept.alsabbagh_institute.server.APIs;
import com.techConcept.alsabbagh_institute.server.ServerManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import techContept.theinstitute.R;


public class NotificationsFragment extends Fragment {

    View rootView;
    LinearLayout progressLL;
    RecyclerView notificationsRV;
    BasicData basicData;
    ArrayList<Notification> readNotifications;
    ArrayList<Notification> newNotifications;
    TextView notificationTV;
    TextView noNotesTV;
    ImageView notNotesIV;
    ImageView moreNotificationsIV;
    NotificationAdapter notificationAdapter;
    boolean allNotifications = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_notifications, container, false);
        progressLL = rootView.findViewById(R.id.progress);
        notificationsRV = rootView.findViewById(R.id.notificationsRV);
        notificationTV = rootView.findViewById(R.id.notificationTV);
        noNotesTV = rootView.findViewById(R.id.notNotesTV);
        notNotesIV = rootView.findViewById(R.id.notNotesIV);
        moreNotificationsIV = rootView.findViewById(R.id.moreNotificationsIV);
        basicData = BasicData.getInstance(getContext());
//        basicData.setGradientColor(notificationTV);
        notificationsRV.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));
        readNotifications = new ArrayList<>();
        newNotifications = new ArrayList<>();
        progressLL.setVisibility(View.VISIBLE);
        moreNotificationsIV.setOnClickListener(v -> {
            if (!allNotifications) {
                newNotifications.addAll(readNotifications);
                notificationsRV.setVisibility(View.VISIBLE);
                allNotifications = true;
                notificationAdapter.notifyDataSetChanged();
                moreNotificationsIV.setImageResource(R.drawable.ic_less_nots);
            } else {
                newNotifications.removeAll(readNotifications);
                if (newNotifications.size() == 0) {
                    notificationsRV.setVisibility(View.GONE);
                }
                notificationAdapter.notifyDataSetChanged();
                allNotifications = false;
                moreNotificationsIV.setImageResource(R.drawable.ic_more_nots);
            }
        });
        ServerManager.get(getContext(), APIs.getAll, basicData.getTOKEN(), progressLL, result -> {
            try {
                JSONArray data = new JSONArray(new JSONObject(new JSONObject(result).getString("data")).getString("rows"));
                for (int i = 0; i < data.length(); i++) {
                    if (data.getJSONObject(i).getBoolean("isRead")) {
                        readNotifications.add(new Notification(data.getJSONObject(i).getString("id"),
                                data.getJSONObject(i).getString("title"),
                                data.getJSONObject(i).getString("body"),
                                data.getJSONObject(i).getString("sectionId"),
                                data.getJSONObject(i).getBoolean("isRead")));
                    } else {
                        newNotifications.add(new Notification(data.getJSONObject(i).getString("id"),
                                data.getJSONObject(i).getString("title"),
                                data.getJSONObject(i).getString("body"),
                                data.getJSONObject(i).getString("sectionId"),
                                data.getJSONObject(i).getBoolean("isRead")));
                    }
                }
                progressLL.setVisibility(View.GONE);
                if (data.length() != 0) {
                    notificationTV.setVisibility(View.VISIBLE);
                    if (newNotifications.size() != 0) {
                        notificationAdapter = new NotificationAdapter(getContext(), newNotifications);
                        notificationsRV.setVisibility(View.VISIBLE);
                    } else {
                        notificationAdapter = new NotificationAdapter(getContext(), readNotifications);
                    }
                    notificationsRV.setAdapter(notificationAdapter);
                    if (readNotifications.size() != 0) {
                        moreNotificationsIV.setVisibility(View.VISIBLE);
                    }
                } else {
                    notNotesIV.setVisibility(View.VISIBLE);
                    noNotesTV.setVisibility(View.VISIBLE);
                    basicData.setGradientColor(noNotesTV);
                }
                if (newNotifications.size() != 0) {
                    ServerManager.put(getContext(), APIs.update, null, basicData.getTOKEN(),
                            progressLL, result1 -> {
                                Log.e("DONE", "DONE");
                            });
                }
            } catch (Exception e) {
                progressLL.setVisibility(View.GONE);
                Log.e("EXE", e.getMessage());
            }
        });

        return rootView;
    }
}