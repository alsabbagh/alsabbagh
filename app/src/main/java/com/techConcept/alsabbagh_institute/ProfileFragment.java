package com.techConcept.alsabbagh_institute;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.techConcept.alsabbagh_institute.server.APIs;
import com.techConcept.alsabbagh_institute.server.ServerManager;

import org.json.JSONObject;

import java.util.Objects;
import techContept.theinstitute.R;


public class ProfileFragment extends Fragment {

    private View rootView;
    private Button moreInfoBTN;
    private Button updatePasswordBTN;
    private EditText fullName;
    private EditText username;
    private EditText phone;
    private EditText mobilePhone;
    private EditText fatherPhone;
    private EditText motherPhone;
    private boolean areHidden;
    private BasicData basicData;
    private TextView otherInfoTV;
    private ImageView logoutIV;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        initViews();

        logoutIV.setOnClickListener(v -> {
            new AlertDialog.Builder(getContext())
                    .setMessage(getContext().getResources().getString(R.string.sure_logout))
                    .setNegativeButton(getContext().getResources().getString(R.string.no), null)
                    .setPositiveButton(getContext().getResources().getString(R.string.yes), (dialog, which) -> {
                        basicData.setTOKEN(null);
                        basicData.setUserData(null);
                        basicData.setProgram(null);
                        basicData.setServerProgram(null);
                        Objects.requireNonNull(getActivity()).finish();
                        startActivity(new Intent(getContext(), LoginActivity.class));
                    }).show();
        });

        moreInfoBTN.setOnClickListener(v -> {
            if (areHidden) {
                Animation animation =
                        new TranslateAnimation(0, 0, 500, 0);
                animation.setDuration(1000);
                animation.setFillAfter(true);
                phone.setAnimation(animation);
                mobilePhone.setAnimation(animation);
                fatherPhone.setAnimation(animation);
                motherPhone.setAnimation(animation);
                phone.setVisibility(View.VISIBLE);
                mobilePhone.setVisibility(View.VISIBLE);
                fatherPhone.setVisibility(View.VISIBLE);
                motherPhone.setVisibility(View.VISIBLE);
                otherInfoTV.setVisibility(View.VISIBLE);
                updatePasswordBTN.setVisibility(View.VISIBLE);
                moreInfoBTN.setText(R.string.less);
                areHidden = false;
            } else {
                Animation animation1 =
                        new TranslateAnimation(0, 0, 0, 500);
                animation1.setDuration(1000);
                animation1.setFillAfter(true);
                phone.setAnimation(animation1);
                mobilePhone.setAnimation(animation1);
                fatherPhone.setAnimation(animation1);
                motherPhone.setAnimation(animation1);
                phone.setVisibility(View.GONE);
                mobilePhone.setVisibility(View.GONE);
                fatherPhone.setVisibility(View.GONE);
                motherPhone.setVisibility(View.GONE);
                otherInfoTV.setVisibility(View.GONE);
                updatePasswordBTN.setVisibility(View.GONE);
                moreInfoBTN.setText(R.string.more);
                areHidden = true;
            }
        });

        updatePasswordBTN.setOnClickListener(v -> {
            Dialog dialog = new Dialog(getContext());
            dialog.setContentView(R.layout.update_password_dialog);
            dialog.setCancelable(false);
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            Button updateBTN = dialog.findViewById(R.id.updateBTN);
            Button cancelBTN = dialog.findViewById(R.id.cancelBTN);
            EditText oldPassword = dialog.findViewById(R.id.oldPasswordET);
            EditText newPassword = dialog.findViewById(R.id.newPasswordET);
            LinearLayout progress = dialog.findViewById(R.id.progressLL);
            cancelBTN.setOnClickListener(v1 -> dialog.dismiss());
            updateBTN.setOnClickListener(v1 -> {
                if (newPassword.getText().toString().trim().isEmpty() ||
                        oldPassword.getText().toString().trim().isEmpty()) {
                    basicData.createToast(getContext().getResources().getString(R.string.full_info));
                } else {
                    JSONObject requestBody = new JSONObject();
                    progress.setVisibility(View.VISIBLE);
                    try {
                        requestBody.put("oldPassword", oldPassword.getText().toString().trim());
                        requestBody.put("newPassword", newPassword.getText().toString().trim());
                        String request = requestBody.toString();
                        ServerManager.put(getContext(), APIs.updatePassword, request,
                                basicData.getTOKEN(), progress, result -> {
                                    basicData.createToast(getContext().getResources()
                                            .getString(R.string.updated));
                                    dialog.dismiss();
                                });
                    } catch (Exception e) {
                        Log.e("EX", e.getMessage());
                    }
                }
            });
            dialog.show();
        });

        return rootView;
    }


    private void initViews() {
        moreInfoBTN = rootView.findViewById(R.id.moreInfoBTN);
        updatePasswordBTN = rootView.findViewById(R.id.updatePasswordBTN);
        fullName = rootView.findViewById(R.id.fullNameET);
        username = rootView.findViewById(R.id.usernameET);
        phone = rootView.findViewById(R.id.phoneET);
        mobilePhone = rootView.findViewById(R.id.mobilePhoneET);
        fatherPhone = rootView.findViewById(R.id.fatherPhoneET);
        motherPhone = rootView.findViewById(R.id.motherPhoneET);
        otherInfoTV = rootView.findViewById(R.id.other_info);

        logoutIV = rootView.findViewById(R.id.logoutIV);
        areHidden = true;
        basicData = BasicData.getInstance(getContext());

        loadUserInfo();
    }

    private void loadUserInfo() {
        JSONObject userInfo = basicData.getUserData();
        try {
            fullName.setText(userInfo.getString("name"));
            username.setText(userInfo.getString("username"));
            phone.setText(userInfo.getString("phone"));
            motherPhone.setText(userInfo.getString("motherPhone"));
            fatherPhone.setText(userInfo.getString("fatherPhone"));
            mobilePhone.setText(userInfo.getString("mobilePhone"));
        } catch (Exception e) {
            Log.e("JSON_EX", e.getMessage());
        }
    }


//    private boolean checkData() {
//        try {
//            return
//                    !fullName.getText().toString().trim()
//                            .equals(userInfo.getString("name")) ||
//                            !phone.getText().toString().trim()
//                                    .equals(userInfo.getString("phone")) ||
//                            !mobilePhone.getText().toString().trim()
//                                    .equals(userInfo.getString("mobilePhone"));
//        } catch (JSONException e) {
//            e.printStackTrace();
//            return false;
//        }
//    }


//        updateBTN.setOnClickListener(v -> {
//            if (checkData()) {
//                progressLL.setVisibility(View.VISIBLE);
//                JSONObject newData = new JSONObject();
//                try {
//                    newData.put("name", fullName.getText().toString().trim());
//                    newData.put("phone", phone.getText().toString().trim());
//                    newData.put("mobilePhone", mobilePhone.getText().toString().trim());
//                    newData.put("mobilePhone", mobilePhone.getText().toString().trim());
//                    newData.put("mobilePhone", mobilePhone.getText().toString().trim());
//                    String requestBody = newData.toString();
//                    ServerManager.put(getContext(), APIs.updateProfile, requestBody, basicData.getTOKEN(),
//                            progressLL, result -> {
//                                try {
//                                    newData.put("username", username.getText().toString());
//                                    basicData.setUserData(newData);
//                                    basicData.createToast(getContext().getResources()
//                                            .getString(R.string.updated));
//                                    progressLL.setVisibility(View.GONE);
//                                } catch (Exception e) {
//                                    Log.e("Exception", e.getMessage());
//                                }
//                            });
//                } catch (Exception e) {
//                    Log.e("JSON_EX", e.getMessage());
//                }
//            } else {
//                basicData.createToast(getContext().getResources().getString(R.string.no_thing_updated));
//            }
//        });


}