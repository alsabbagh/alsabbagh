package com.techConcept.alsabbagh_institute;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.techConcept.alsabbagh_institute.adapters.TestAdapter;
import com.techConcept.alsabbagh_institute.models.Test;
import com.techConcept.alsabbagh_institute.server.APIs;
import com.techConcept.alsabbagh_institute.server.ServerManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import techContept.theinstitute.R;
public class CourseDetailsActivity extends AppCompatActivity {

    private RecyclerView testsRV;
    private LinearLayout progress;
    private TextView courseTV;
    private ArrayList<Test> tests;
    private BasicData basicData;
    private String sectionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_details);
        progress = findViewById(R.id.progressLL);
        testsRV = findViewById(R.id.testsRV);
        courseTV = findViewById(R.id.courseTV);

        basicData = BasicData.getInstance(this);
        sectionId = (String) getIntent().getSerializableExtra("id");
        testsRV.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        testsRV.setHasFixedSize(true);
        getTests();
    }

    private void getTests() {
        tests = new ArrayList<>();
        progress.setVisibility(View.VISIBLE);
        ServerManager.get(this, APIs.getTests + "?sectionId=" + sectionId,
                basicData.getTOKEN(), progress, result -> {
                    try {
                        JSONArray data = new JSONArray(new JSONObject(result).getString("data"));
                        courseTV.setText(data.getJSONObject(0).getJSONObject("test").getJSONObject("section").getString("title"));
                        for (int i = 0; i < data.length(); i++) {
                            tests.add(new Test(data.getJSONObject(i).getJSONObject("test").getString("title"),
                                    data.getJSONObject(i).getJSONObject("test").getJSONObject("course").getString("title"),
                                    data.getJSONObject(i).getJSONObject("test").getString("date"),
                                    data.getJSONObject(i).getInt("mark"),
                                    data.getJSONObject(i).getJSONObject("test").getInt("mark")));
                        }
                        progress.setVisibility(View.GONE);
                        if (tests.size() != 0) {
                            testsRV.setAdapter(new TestAdapter(this, tests));
                        } else {
                            basicData.createToast(getResources().getString(R.string.no_tests_yet));
                        }
                    } catch (Exception e) {
                        progress.setVisibility(View.GONE);
                        Log.e("EXE", e.getMessage());
                    }
                });
    }
}