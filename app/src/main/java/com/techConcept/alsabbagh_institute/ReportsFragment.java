package com.techConcept.alsabbagh_institute;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.techConcept.alsabbagh_institute.adapters.ReportAdapter;
import com.techConcept.alsabbagh_institute.models.Report;
import com.techConcept.alsabbagh_institute.server.APIs;
import com.techConcept.alsabbagh_institute.server.ServerManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import techContept.theinstitute.R;

public class ReportsFragment extends Fragment {

    View rootView;
    Button reportBTN;
    RecyclerView reportsRV;
    BasicData basicData;
    LinearLayout progressLL;
    ArrayList<Report> reports;
    ReportAdapter reportAdapter;
    TextView reportsTV;
    TextView noReportsTV;
    ImageView noReportIV;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_reports, container, false);
        reportBTN = rootView.findViewById(R.id.addReportBTN);
        reportsRV = rootView.findViewById(R.id.reportsRV);
        progressLL = rootView.findViewById(R.id.progressLL);
        reportsTV = rootView.findViewById(R.id.reportsTV);
        noReportsTV = rootView.findViewById(R.id.notReportsTV);
        noReportIV = rootView.findViewById(R.id.notReportsIV);
        basicData = BasicData.getInstance(getContext());
//        basicData.setGradientColor(reportsTV);
        reports = new ArrayList<>();
        reportsRV.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));
        reportsRV.setHasFixedSize(false);
        getReports();

        reportBTN.setOnClickListener(v -> showAddReportDialog());
        return rootView;
    }

    private void showAddReportDialog() {
        Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.send_report_dialog);
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        EditText reportET = dialog.findViewById(R.id.reportBodyET);
        Button sendReportBTN = dialog.findViewById(R.id.sendReportBTN);
        Button cancelBTN = dialog.findViewById(R.id.cancelBTN);
        LinearLayout progress = dialog.findViewById(R.id.progressLL);
        cancelBTN.setOnClickListener(v1 -> dialog.dismiss());

        sendReportBTN.setOnClickListener(v1 -> {
            if (reportET.getText().toString().trim().isEmpty()) {
                basicData.createToast(getContext().getResources().getString(R.string.enter_report));
            } else {
                progress.setVisibility(View.VISIBLE);
                JSONObject jsonBody = new JSONObject();
                try {
                    jsonBody.put("body", reportET.getText().toString().trim());
                    final String requestBody = jsonBody.toString();
                    ServerManager.post(getContext(), APIs.add, requestBody, basicData.getTOKEN(),
                            progress, result -> {
                                try {
                                    JSONObject data =
                                            new JSONObject(new JSONObject(result).getString("data"));
                                    progress.setVisibility(View.GONE);
                                    Report report = new Report(data.getString("id"),
                                            data.getString("body"), data.getString("date"));
                                    reports.add(0, report);
                                    if (reports.size() == 1) {
                                        noReportIV.setVisibility(View.GONE);
                                        noReportsTV.setVisibility(View.GONE);
                                    }
                                    reportAdapter.notifyDataSetChanged();
                                    dialog.dismiss();
                                } catch (JSONException e) {
                                    progress.setVisibility(View.GONE);
                                    e.printStackTrace();
                                }
                            });
                } catch (Exception e) {
                    progressLL.setVisibility(View.GONE);
                }
            }
        });
        dialog.show();
    }

    private void getReports() {
        progressLL.setVisibility(View.VISIBLE);
        ServerManager.get(getContext(), APIs.getReports, basicData.getTOKEN(), progressLL, result -> {
            try {
                JSONArray data = new JSONArray(new JSONObject(result).getString("data"));
                for (int i = 0; i < data.length(); i++) {
                    reports.add(new Report(data.getJSONObject(i).getString("id"),
                            data.getJSONObject(i).getString("body"),
                            data.getJSONObject(i).getString("date")));
                }
                progressLL.setVisibility(View.GONE);
                reportAdapter = new ReportAdapter(getContext(), reports);
                reportsRV.setAdapter(reportAdapter);
                reportBTN.setVisibility(View.VISIBLE);
                if (reports.size() == 0) {
                    noReportIV.setVisibility(View.VISIBLE);
                    noReportsTV.setVisibility(View.VISIBLE);
                    basicData.setGradientColor(noReportsTV);
                } else {
                    reportsTV.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }
}