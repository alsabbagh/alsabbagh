package com.techConcept.alsabbagh_institute;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.techConcept.alsabbagh_institute.adapters.CourseAdapter;
import com.techConcept.alsabbagh_institute.models.Section;
import com.techConcept.alsabbagh_institute.server.APIs;
import com.techConcept.alsabbagh_institute.server.ServerManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import techContept.theinstitute.R;

public class SectionsFragment extends Fragment {


    View rootView;
    RecyclerView coursesRV;
    ArrayList<Section> sections;
    Button addSectionBTN;
    TextView noSectionsTV;
    LinearLayout progress;
    BasicData basicData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_sections, container, false);

        coursesRV = rootView.findViewById(R.id.coursesRV);
        addSectionBTN = rootView.findViewById(R.id.addSectionBTN);
        noSectionsTV = rootView.findViewById(R.id.noSectionsTV);
        progress = rootView.findViewById(R.id.progressLL);
        basicData = BasicData.getInstance(getContext());

        addSectionBTN.setOnClickListener(v ->
                startActivity(new Intent(getContext(), AddSectionActivity.class))
        );

        coursesRV.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();

        sections = new ArrayList<>();
        getSections();
    }

    private void getSections() {
        progress.setVisibility(View.VISIBLE);
        ServerManager.get(getContext(), APIs.sections + "?isAccept=true",
                basicData.getTOKEN(), progress, result -> {
                    try {
                        JSONArray data = new JSONArray(new JSONObject(result)
                                .getString("data"));
                        for (int i = 0; i < data.length(); i++) {
                            sections.add(new Section(
                                    data.getJSONObject(i).getJSONObject("section").getString("id"),
                                    data.getJSONObject(i).getJSONObject("section").getString("title"),
                                    data.getJSONObject(i).getJSONObject("section").getString("description"),
                                    data.getJSONObject(i).getString("id")));

                        }
                        progress.setVisibility(View.GONE);

                        if (sections.size() == 0) {
                            noSectionsTV.setVisibility(View.VISIBLE);
                            return;
                        }

                        noSectionsTV.setVisibility(View.GONE);

                        coursesRV.setAdapter(new CourseAdapter(getContext(), sections));
                    } catch (JSONException e) {
                        progress.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                });
    }

}