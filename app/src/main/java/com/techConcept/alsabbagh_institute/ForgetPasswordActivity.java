package com.techConcept.alsabbagh_institute;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import techContept.theinstitute.R;

public class ForgetPasswordActivity extends AppCompatActivity {

    private EditText emailET;
    private Button sendCodeBTN;
    private TextView backTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        emailET = findViewById(R.id.emailET);
        sendCodeBTN = findViewById(R.id.sendCodeBTN);
        backTV = findViewById(R.id.backTV);

        backTV.setOnClickListener(v -> {
            finish();
        });

        sendCodeBTN.setOnClickListener(v -> {
            if (emailET.getText().toString().trim().isEmpty()) {
                Snackbar.make(this.sendCodeBTN, R.string.enter_email, Snackbar.LENGTH_LONG)
                        .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                        .setBackgroundTint(getResources().getColor(R.color.dark_blue))
                        .show();
            } else {
                
            }
        });

    }
}