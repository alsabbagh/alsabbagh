package com.techConcept.alsabbagh_institute;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import techContept.theinstitute.R;


public class SplashScreenActivity extends AppCompatActivity {

    BasicData basicData;
    ImageView logoIV;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        basicData = BasicData.getInstance(this);
        linearLayout = findViewById(R.id.progressLL);
        logoIV = findViewById(R.id.logoIV);
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator());
        fadeIn.setDuration(2000);
        logoIV.startAnimation(fadeIn);

        new Handler().postDelayed(() -> {
            if (basicData.getTOKEN() == null) {
                startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
            } else {
                Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(intent);
            }
            finish();
        }, 2100);
    }

}