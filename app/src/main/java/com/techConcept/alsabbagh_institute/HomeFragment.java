package com.techConcept.alsabbagh_institute;

import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.techConcept.alsabbagh_institute.models.Program;
import com.techConcept.alsabbagh_institute.models.Story;
import com.techConcept.alsabbagh_institute.server.APIs;
import com.techConcept.alsabbagh_institute.server.ServerManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;
import java.util.TimeZone;

import omari.hamza.storyview.StoryView;
import omari.hamza.storyview.model.MyStory;
import techContept.theinstitute.R;

public class HomeFragment extends Fragment {
    View rootView;
    ArrayList<Story> stories;
    LinearLayout progress;
    CardView storiesLL;
    LinearLayout imageProgressLL;
    BasicData basicData;
    ImageView storyIV;
    Button saveProgram;
    String appVersion;
    Button doneBTN;
    ArrayList<MyStory> myStories;
    private TableLayout programTable;
    private ScrollView mainTableLayout;
    private ArrayList<Program> program;
    private ArrayList<Program> extraProgram;


    String days[] = new String[]{
            "السبت", "الأحد", "الإثنين", "الثلاثاء", "الاربعاء", "الخميس", "الجمعة"
    };

    String hours[] = new String[]{
            "", "9-10", "10-11", "11-12", "12-1", "1-2", "2-3", "3-4", "4-5", "5-6", "6-7", "7-8", "8-9"
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        progress = rootView.findViewById(R.id.progressLL);
        imageProgressLL = rootView.findViewById(R.id.imageProgressLL);
        storiesLL = rootView.findViewById(R.id.storiesLL);
        storyIV = rootView.findViewById(R.id.storyImage);
        programTable = rootView.findViewById(R.id.programTable);
        mainTableLayout = rootView.findViewById(R.id.mainTableLayout);
        saveProgram = rootView.findViewById(R.id.saveProgram);
        programTable.setStretchAllColumns(true);
        programTable.bringToFront();
        basicData = BasicData.getInstance(getContext());

        appVersion = "2.1.0";

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Damascus"));
        calendar.add(Calendar.HOUR, 0);

        MyVectorClock vectorAnalogClock = rootView.findViewById(R.id.clock);

        //customization
        vectorAnalogClock.setCalendar(calendar)
//                .setDiameterInDp(170.0f)
                .setOpacity(1.0f)
                .setShowSeconds(true)
                .setColor(getResources().getColor(R.color.brown));

        myStories = new ArrayList<>();
        program = new ArrayList<>();
        extraProgram = new ArrayList<>();

        loadLocalProgram();
        getAppVersion();
        setTableData();
        getStories();

        storyIV.setOnClickListener(v -> {
            if (myStories.size() > 0) {
                new StoryView.Builder(Objects.requireNonNull(getActivity()).getSupportFragmentManager())
                        .setStoriesList(myStories)
                        .setStoryDuration(6000)
                        .setTitleLogoUrl(ServerManager.ROOT + "/uploads/logo.png")
                        .build()
                        .show();


                ServerManager.get(getContext(), APIs.userStory,
                        basicData.getTOKEN(), null, result -> {

                        });
            }
        });

        saveProgram.setOnClickListener(v -> {
            basicData.setServerProgram(extraProgram);
            basicData.createToast("تم الحفظ بنجاح!");
        });

        return rootView;
    }

    public void getImage(String path) {

        imageProgressLL.setVisibility(View.VISIBLE);

        Picasso.get()
                .load(path)
                .resize(50, 50)
                .centerCrop()
                .into(storyIV, new Callback() {
                    @Override
                    public void onSuccess() {
                        imageProgressLL.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        imageProgressLL.setVisibility(View.GONE);
                    }
                });
    }

    private void getStories() {
        stories = new ArrayList<>();

        ServerManager.get(getContext(), APIs.stories,
                basicData.getTOKEN(), null, result -> {
                    try {
                        JSONArray data = new JSONArray(new
                                JSONObject(new JSONObject(result).getString("data")).getString("rows"));

                        for (int i = 0; i < data.length(); i++) {
                            stories.add(new Story(
                                    data.getJSONObject(i).getString("id"),
                                    data.getJSONObject(i).getString("post"),
                                    data.getJSONObject(i).getString("image"),
                                    data.getJSONObject(i).getString("createdAt")));
                        }


                        for (int i = 0; i < stories.size(); i++) {
//                            SimpleDateFormat format = new SimpleDateFormat(
//                                    "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
//                            format.setTimeZone(TimeZone.getTimeZone("UTC"));
//
//                            Date date = format.parse(stories.get(i).getCreatedAt());

                            myStories.add(new MyStory(ServerManager.ROOT + stories.get(i).getImage(),
                                    null,
                                    stories.get(i).getPost()));
                        }

                        for (int i = 0; i < stories.size(); i++) {
                            if (stories.get(i).getImage() != null || !stories.get(i).getImage().equals("")) {
                                getImage(ServerManager.ROOT + stories.get(i).getImage());
                                break;
                            }
                        }

                        if (stories.size() == 0) {
                            imageProgressLL.setVisibility(View.GONE);
                            storyIV.setImageResource(R.drawable.no_image);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                });
    }

    private void setTableData() {
        progress.setVisibility(View.VISIBLE);

        if (extraProgram != null && extraProgram.size() > 0) {
            setData(extraProgram);
        }

        ServerManager.get(getContext(), APIs.program,
                basicData.getTOKEN(), progress, result -> {
                    try {
                        program = new ArrayList<>();
                        mainTableLayout.setVisibility(View.VISIBLE);
                        saveProgram.setVisibility(View.VISIBLE);

                        JSONArray data = new JSONArray(new JSONObject(result)
                                .getString("data"));
                        for (int y = 0; y < data.length(); y++) {
                            program.add(new Program(
                                    data.getJSONObject(y).getString("day"),
                                    data.getJSONObject(y).getString("time"),
                                    data.getJSONObject(y).getString("section"),
                                    false));
                        }

                        ArrayList<Program> fullProgram = new ArrayList<>();
                        if (extraProgram == null) extraProgram = new ArrayList<>();

                        ArrayList<Program> localProgram = new ArrayList<>();

                        for (Program program: extraProgram) {
                            if (program.isFocusable()) {
                                localProgram.add(program);
                            }
                        }

                        fullProgram.addAll(localProgram);
                        fullProgram.addAll(program);

                        basicData.setServerProgram(fullProgram);

                        int count = programTable.getChildCount();
                        for (int i = 1; i < count; i++) {
                            View child = programTable.getChildAt(i);
                            if (child instanceof TableRow) ((ViewGroup) child).removeAllViews();
                        }

                        setData(fullProgram);

                        progress.setVisibility(View.GONE);

                    } catch (JSONException e) {
                        progress.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                });

    }

    private void setCellStyle(TextView day) {
        day.setTextColor(getResources().getColor(R.color.brown));
        day.setTextSize(getResources().getDimension(R.dimen.small));
        day.setGravity(Gravity.CENTER);
        day.setPadding(7, 25, 7, 25);
        Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.jf4);
        day.setTypeface(typeface);
    }

    private void setCellStyle(EditText day) {
        day.setTextColor(getResources().getColor(R.color.brown));
        day.setTextSize(getResources().getDimension(R.dimen.small));
        day.setGravity(Gravity.CENTER);
        day.setPadding(7, 25, 7, 25);
        Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.jf4);
        day.setTypeface(typeface);
    }

    private void loadLocalProgram() {
        try {
            extraProgram = basicData.getServerProgram();
        } catch (Exception e) {
            Log.e("JSON_EX", e.getMessage());
        }
    }

    private void getAppVersion() {

        ServerManager.get(getContext(), APIs.version,
                null, null, result -> {
                    try {
                        JSONObject data = new JSONObject(result);

                        String code = data.getString("data");

                        if (!code.equals(appVersion)) {
                            Dialog dialog = new Dialog(getContext());
                            dialog.setContentView(R.layout.update_app_dialog);
                            dialog.setCancelable(false);
                            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

                            doneBTN = dialog.findViewById(R.id.doneBTN);

                            doneBTN.setOnClickListener(v ->
                                    dialog.dismiss()
                            );

                            dialog.show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                });
    }

    private void setData(ArrayList<Program> fullProgram) {

        for (int i = 0; i < days.length; i++) {
            TableRow tr = new TableRow(getContext());
            tr.setBackgroundColor(getResources().getColor(R.color.light_white));

            for (int j = 0; j < hours.length; j++) {
                Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Damascus"));

                int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

                if (j == 0) {
                    TextView day = new TextView(getContext());
                    day.setBackground(getResources().getDrawable(R.drawable.table_borders));
                    day.setText(days[i]);
                    setCellStyle(day);
                    tr.addView(day);

                    if (dayOfWeek == i)
                        day.setTextColor(getResources().getColor(R.color.blue));

                } else {
                    EditText day = new EditText(getContext());

                    if (fullProgram.size() == 0) {
                        day.setBackground(getResources().getDrawable(R.drawable.table_borders));
                        setCellStyle(day);
                        day.setText("");
                    } else {
                        for (int k = 0; k < fullProgram.size(); k++) {
                            day.setBackground(getResources().getDrawable(R.drawable.table_borders));
                            setCellStyle(day);
                            if (dayOfWeek == i)
                                day.setTextColor(getResources().getColor(R.color.blue));

                            if (fullProgram.get(k).getDay().contains(days[i]) && fullProgram.get(k).getHour().equals(hours[j])) {
                                day.setText(fullProgram.get(k).getSection());
                                day.setFocusable(fullProgram.get(k).isFocusable());
                                break;
                            } else {
                                day.setText(" ");
                            }
                        }
                    }

                    int finalI = i;
                    int finalJ = j;
                    day.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            Program program = new Program(days[finalI], hours[finalJ],
                                    day.getText().toString().trim(), true);

                            ArrayList<Program> temporaryProgram = new ArrayList<>(extraProgram);

                            int isFound = -1;

                            for (int iterator = 0; iterator < extraProgram.size(); iterator++) {
                                if (extraProgram.get(iterator).getDay().equals(days[finalI]) &&
                                        extraProgram.get(iterator).getHour().equals(hours[finalJ])) {
                                    isFound = iterator;
                                }
                            }

                            if (isFound > -1) {
                                temporaryProgram.remove(isFound);
                                temporaryProgram.add(isFound, program);
                            } else {
                                temporaryProgram.add(program);
                            }
                            extraProgram.clear();
                            extraProgram.addAll(temporaryProgram);
                        }
                    });
                    tr.addView(day);
                }
            }

            programTable.addView(tr, new TableLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
        }

    }

}