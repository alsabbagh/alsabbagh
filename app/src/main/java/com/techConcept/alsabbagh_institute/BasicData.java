package com.techConcept.alsabbagh_institute;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.text.TextPaint;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.techConcept.alsabbagh_institute.models.Program;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import techContept.theinstitute.R;

public class BasicData {

    @SuppressLint("StaticFieldLeak")
    private static BasicData basicData = null;
    private Context context;
    private final String USER_DATA = "USER_DATA";
    private final String TOKEN = "TOKEN";
    private final String PROGRAM = "PROGRAM";
    private final String SERVER_PROGRAM = "SERVER_PROGRAM";
    private Gson gson = null;

    private BasicData() {

    }

    public static BasicData getInstance(Context context) {
        if (basicData == null) {
            basicData = new BasicData();
            basicData.context = context;
            basicData.gson = new Gson();
            return basicData;
        }
        return basicData;
    }

    public void setGradientColor(TextView welcomeTV) {
        TextPaint paint = welcomeTV.getPaint();
        float width = paint.measureText(welcomeTV.getText().toString());
        Shader textShader = new LinearGradient(0, 0, width, welcomeTV.getTextSize(),
                new int[]{
                        Color.parseColor(String.valueOf("#1F7CB5")),
                        Color.parseColor(String.valueOf("#1E2B4E")),
                }, null, Shader.TileMode.CLAMP);
        welcomeTV.getPaint().setShader(textShader);
    }

    public JSONObject getUserData() {
        android.content.SharedPreferences pref = context
                .getSharedPreferences(context.getApplicationInfo().name, Context.MODE_PRIVATE);
        try {
            return new JSONObject(pref.getString(USER_DATA, null));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setUserData(JSONObject user) {
        android.content.SharedPreferences.Editor pref = context
                .getSharedPreferences(context.getApplicationInfo().name, Context.MODE_PRIVATE).edit();
        pref.putString(USER_DATA, String.valueOf(user)).apply();
    }

    public String getTOKEN() {
        android.content.SharedPreferences pref = context
                .getSharedPreferences(context.getApplicationInfo().name, Context.MODE_PRIVATE);
        return pref.getString(TOKEN, null);
    }

    public void setTOKEN(String token) {
        SharedPreferences.Editor pref = context.getSharedPreferences(context.getApplicationInfo().name,
                Context.MODE_PRIVATE).edit();
        pref.putString(TOKEN, token).apply();
    }


    public void setProgram(ArrayList<Program> program) {
        SharedPreferences.Editor pref = context.getSharedPreferences(context.getApplicationInfo().name,
                Context.MODE_PRIVATE).edit();
        Gson gson = new Gson();
        String json = gson.toJson(program);
        pref.putString(PROGRAM, json);
        pref.apply();
    }

    public void setServerProgram(ArrayList<Program> program) {
        SharedPreferences.Editor pref = context.getSharedPreferences(context.getApplicationInfo().name,
                Context.MODE_PRIVATE).edit();
        Gson gson = new Gson();
        String json = gson.toJson(program);
        pref.putString(SERVER_PROGRAM, json);
        pref.apply();
    }

    public ArrayList<Program> getServerProgram() {
        android.content.SharedPreferences pref = context
                .getSharedPreferences(context.getApplicationInfo().name, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = pref.getString(SERVER_PROGRAM, null);
        Type type = new TypeToken<ArrayList<Program>>() {}.getType();
        return gson.fromJson(json, type);
    }



    public ArrayList<Program> getProgram() {
        android.content.SharedPreferences pref = context
                .getSharedPreferences(context.getApplicationInfo().name, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = pref.getString(PROGRAM, null);
        Type type = new TypeToken<ArrayList<Program>>() {}.getType();
        return gson.fromJson(json, type);
    }


    public void createToast(String message) {
        Toast toast = new Toast(context);
        @SuppressLint("InflateParams") View view = LayoutInflater.from(context)
                .inflate(R.layout.custom_toast, null);
        TextView textView = view.findViewById(R.id.custom_toast_text);
        textView.setText(message);
        toast.setView(view);
        toast.setGravity(Gravity.BOTTOM | Gravity.CENTER, 0, 5);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }

    public void createLongToast(String message) {
        Toast toast = new Toast(context);
        @SuppressLint("InflateParams") View view = LayoutInflater.from(context)
                .inflate(R.layout.custom_toast, null);
        TextView textView = view.findViewById(R.id.custom_toast_text);
        textView.setText(message);
        toast.setView(view);
        toast.setGravity(Gravity.BOTTOM | Gravity.CENTER, 0, 5);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }
}
