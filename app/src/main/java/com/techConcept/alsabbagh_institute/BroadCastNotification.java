package com.techConcept.alsabbagh_institute;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BroadCastNotification extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startActivity(new Intent(context, MainActivity.class).putExtra("notify", true));
    }
}
