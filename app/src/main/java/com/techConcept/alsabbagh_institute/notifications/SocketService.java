//package com.techConcept.alsabbagh_institute.notifications;
//
//import android.app.NotificationChannel;
//import android.app.PendingIntent;
//import android.app.Service;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.BitmapFactory;
//import android.os.Build;
//import android.os.IBinder;
//import android.util.Log;
//import android.widget.LinearLayout;
//
//import androidx.annotation.Nullable;
//import androidx.annotation.RequiresApi;
//import androidx.core.app.NotificationCompat;
//
//import com.techConcept.alsabbagh_institute.BasicData;
//import com.techConcept.alsabbagh_institute.R;
//import com.techConcept.alsabbagh_institute.SplashScreenActivity;
//import com.techConcept.alsabbagh_institute.server.APIs;
//import com.techConcept.alsabbagh_institute.server.ServerManager;
//
//import org.json.JSONException;
//
//import java.net.URISyntaxException;
//import java.util.Random;
//
//public class SocketService extends Service {
//
//    @Nullable
//    @Override
//    public IBinder onBind(Intent intent) {
//        return null;
//    }
//
//    BasicData basicData;
//
//    public static boolean isRunning;
//
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        try {
//            isRunning = true;
//            basicData = BasicData.getInstance(this);
//            Socket socket = IO.socket(ServerManager.ROOT);
//            socket.on(basicData.getUserData().getString("username"), args -> {
//                try {
////                    String title = new JSONObject(String.valueOf(args[0])).getString("title");
//                    String title = "معهد الصباغ";
//                    String message = "إشعار جديد";
//                    NotificationManager manager = new NotificationManager(title, message, this);
//                    manager.showPopUp();
//                } catch (Exception e) {
//                    Log.e("Sockett", e.getMessage());
//                }
//            });
//
////            socket.on("check", args -> {
////                LinearLayout linearLayout = null;
////                ServerManager.get(this, APIs.getNew, basicData.getTOKEN(), linearLayout,
////                        result -> {
////                        });
////            });
//
//            socket.connect();
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                startForegroundSocket();
//            }
//            return START_STICKY;
//        } catch (Exception e) {
//            Log.e("socket", e.getMessage());
//        }
//        return super.onStartCommand(intent, flags, startId);
//    }
//
//    @RequiresApi(Build.VERSION_CODES.O)
//    private void startForegroundSocket() {
//        android.app.NotificationManager notificationManager =
//                (android.app.NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        NotificationCompat.Builder builder = new
//                NotificationCompat.Builder(this, this.getString(R.string.app_name));
//        builder.setContentTitle(getString(R.string.app_name));
//        builder.setSmallIcon(R.mipmap.ic_launcher);
//        builder.setLargeIcon(
//                BitmapFactory.decodeResource(
//                        getResources(),
//                        R.mipmap.ic_launcher
//                )
//        );
//        Intent intent = new Intent(this, SplashScreenActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
//        intent.putExtra("type2", "home");
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
//                intent, PendingIntent.FLAG_UPDATE_CURRENT);
//        builder.setContentIntent(pendingIntent);
//        builder.setPriority(NotificationCompat.PRIORITY_MIN);
//        NotificationChannel channel = new NotificationChannel(
//                "ID",
//                getString(R.string.app_name),
//                android.app.NotificationManager.IMPORTANCE_MIN
//        );
//        channel.setSound(null, null);
//        channel.setShowBadge(false);
//        builder.setChannelId("ID");
//        notificationManager.createNotificationChannel(channel);
//        startForeground(
//                new Random().nextInt(),
//                builder.build()
//        );
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        isRunning = false;
//    }
//}
