package com.techConcept.alsabbagh_institute.notifications;

import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import androidx.core.app.NotificationCompat;

import com.techConcept.alsabbagh_institute.MainActivity;

import java.util.Random;

import techContept.theinstitute.R;

public class NotificationManager {

    String title;
    String message;
    Context context;

    public NotificationManager(String title, String message, Context context) {
        this.title = title;
        this.message = message;
        this.context = context;
    }

    public void showPopUp() {
        android.app.NotificationManager manager = (android.app.NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder compat =
                new NotificationCompat.Builder(context, context.getString(R.string.app_name));
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("type2", "home");
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        compat.setContentIntent(pendingIntent);
        compat.setContentTitle(title);
        compat.setContentText(message);
        compat.setAutoCancel(true);
        compat.setDefaults(1);
        compat.setSmallIcon(R.mipmap.ic_launcher);
        compat.setLargeIcon(
                BitmapFactory.decodeResource(
                        context.getResources(),
                        R.mipmap.ic_launcher
                )
        );
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    "ID_",
                    context.getString(R.string.app_name),
                    android.app.NotificationManager.IMPORTANCE_HIGH
            );
            channel.enableLights(true);
            channel.setLightColor(Color.BLUE);
            compat.setChannelId("ID_");
            manager.createNotificationChannel(channel);
        }
        manager.notify(new Random().nextInt(), compat.build());
    }
}
