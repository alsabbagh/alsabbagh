package com.techConcept.alsabbagh_institute.models;

public class Notification {

    private String id;
    private String title;
    private String body;
    private String sectionId;
    private boolean isRead;

    public Notification(String id, String title, String body, String sectionId, boolean isRead) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.sectionId = sectionId;
        this.isRead = isRead;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getSectionId() {
        return sectionId;
    }

    public boolean isRead() {
        return isRead;
    }
}
