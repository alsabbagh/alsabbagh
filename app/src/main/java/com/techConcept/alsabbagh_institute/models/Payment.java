package com.techConcept.alsabbagh_institute.models;

public class Payment {
    private int month;
    private int amount;

    public Payment(int month, int amount) {
        this.month = month;
        this.amount = amount;
    }

    public int getMonth() {
        return month;
    }

    public int getAmount() {
        return amount;
    }
}
