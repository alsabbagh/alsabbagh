package com.techConcept.alsabbagh_institute.models;

public class Section {
    private final String id;
    private final String title;
    private final String description;
    private String userSectionRowId;

    public Section(String id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    public Section(String id, String title, String description, String userSectionRowId) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.userSectionRowId = userSectionRowId;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getUserSectionRowId() {
        return userSectionRowId;
    }
}
