package com.techConcept.alsabbagh_institute.models;

public class Program {
    String day;
    String hour;
    String section;
    boolean isFocusable;

    public Program(String day, String hour, String section, boolean isFocusable) {
        this.day = day;
        this.hour = hour;
        this.section = section;
        this.isFocusable = isFocusable;
    }

    public String getDay() {
        return day;
    }

    public String getHour() {
        return hour;
    }

    public String getSection() {
        return section;
    }

    public boolean isFocusable() {
        return isFocusable;
    }
}
