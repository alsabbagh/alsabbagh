package com.techConcept.alsabbagh_institute.models;

public class Story {
    private String id;
    private String post;
    private String image;
    private String createdAt;

    public Story(String id, String post, String image, String createdAt) {
        this.id = id;
        this.post = post;
        this.image = image;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public String getPost() {
        return post;
    }

    public String getImage() {
        return image;
    }

    public String getCreatedAt() {
        return createdAt;
    }
}
