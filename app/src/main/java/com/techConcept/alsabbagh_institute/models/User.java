package com.techConcept.alsabbagh_institute.models;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;
    @SerializedName("email")
    private String email;
    @SerializedName("name")
    private String name;
    @SerializedName("mobilePhone")
    private String mobilePhone;
    @SerializedName("phone")
    private String phone;
    @SerializedName("gender")
    private String gender;
    @SerializedName("address")
    private String address;

    public User() {
    }

    public User(String username, String password, String email,
                String name, String mobilePhone, String phone,
                String gender, String address) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.name = name;
        this.mobilePhone = mobilePhone;
        this.phone = phone;
        this.gender = gender;
        this.address = address;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public String getPhone() {
        return phone;
    }

    public String getGender() {
        return gender;
    }

    public String getAddress() {
        return address;
    }
}
