package com.techConcept.alsabbagh_institute.models;

public class Class {
    private String id;
    private String title;

    public Class(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }


    @Override
    public String toString() {
        return this.title; // What to display in the Spinner list.
    }

}
