package com.techConcept.alsabbagh_institute.models;

public class Test {
    private String title;
    private String course;
    private String date;
    private int mark;
    private int fullMark;

    public Test(String title, String course, String date, int mark, int fullMark) {
        this.title = title;
        this.course = course;
        this.date = date;
        this.mark = mark;
        this.fullMark = fullMark;
    }

    public String getTitle() {
        return title;
    }

    public String getCourse() {
        return course;
    }


    public String getDate() {
        return date;
    }

    public int getMark() {
        return mark;
    }

    public int getFullMark() {
        return fullMark;
    }
}
