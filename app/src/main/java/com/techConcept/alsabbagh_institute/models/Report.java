package com.techConcept.alsabbagh_institute.models;

public class Report {
    private String id;
    private String body;
    private String date;

    public Report(String id, String body, String date) {
        this.id = id;
        this.body = body;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public String getBody() {
        return body;
    }

    public String getDate() {
        return date;
    }
}
