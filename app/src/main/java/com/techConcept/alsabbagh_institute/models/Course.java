package com.techConcept.alsabbagh_institute.models;

public class Course {
    private String id;
    private String title;
    private String instructor;
    private String className;
    private int price;

    public Course(String id, String title, String instructor, String className, int price) {
        this.id = id;
        this.title = title;
        this.instructor = instructor;
        this.className = className;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

}
