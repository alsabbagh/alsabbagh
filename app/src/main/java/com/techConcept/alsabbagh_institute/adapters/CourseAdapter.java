package com.techConcept.alsabbagh_institute.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.techConcept.alsabbagh_institute.BasicData;
import com.techConcept.alsabbagh_institute.CourseDetailsActivity;
import com.techConcept.alsabbagh_institute.models.Section;
import com.techConcept.alsabbagh_institute.server.APIs;
import com.techConcept.alsabbagh_institute.server.ServerManager;

import org.json.JSONObject;

import java.util.ArrayList;

import techContept.theinstitute.R;

public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.ViewHolder> {

    Context context;
    ArrayList<Section> sections;
    boolean showAddBTN;

    public CourseAdapter(Context context, ArrayList<Section> sections, boolean showAddBTN) {
        this.context = context;
        this.sections = sections;
        this.showAddBTN = showAddBTN;
    }

    public CourseAdapter(Context context, ArrayList<Section> sections) {
        this.context = context;
        this.sections = sections;
    }

    @NonNull
    @Override
    public CourseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(context)
                        .inflate(R.layout.course_item,
                                parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull CourseAdapter.ViewHolder holder, int position) {

        if (!showAddBTN) {
            holder.infoIV.setImageResource(R.drawable.ic_delete);
        }

        Section section = sections.get(position);
        holder.titleTV.setText(section.getTitle());
        holder.instructorTV.setText(section.getDescription());
        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(context, CourseDetailsActivity.class);
            intent.putExtra("id", section.getId());
            context.startActivity(intent);
        });

        holder.infoIV.setOnClickListener(v -> {

            BasicData basicData = BasicData.getInstance(v.getContext());

            if (showAddBTN) {

                try {
                    holder.infoIV.setVisibility(View.GONE);
                    holder.progressLL.setVisibility(View.VISIBLE);
                    JSONObject jsonBody = new JSONObject();
                    jsonBody.put("sectionId", section.getId());

                    final String requestBody = jsonBody.toString();
                    ServerManager.post(v.getContext(), APIs.sections, requestBody, basicData.getTOKEN(),
                            null, result -> {
                                holder.infoIV.setVisibility(View.VISIBLE);
                                holder.progressLL.setVisibility(View.GONE);

                                basicData.createToast("تمت الإضافة بنجاح");
                                ((Activity)context).finish();

                            });
                } catch (Exception e) {
                    holder.infoIV.setVisibility(View.VISIBLE);
                    holder.progressLL.setVisibility(View.GONE);
                }
            }

            if (!showAddBTN) {
                new AlertDialog.Builder(context)
                        .setMessage(context.getResources().getString(R.string.remove_section))
                        .setNegativeButton(context.getResources().getString(R.string.no), null)
                        .setPositiveButton(context.getResources().getString(R.string.yes), (dialog, which) -> {
                            String token = BasicData.getInstance(context).getTOKEN();
                            ProgressDialog dialog1 = new ProgressDialog(context);
                            dialog1.setMessage(context.getResources().getString(R.string.removing));
                            dialog1.setCancelable(false);
                            dialog1.show();
                            ServerManager.delete(context, APIs.removeSection + section.getUserSectionRowId(),
                                    token, dialog1,
                                    result -> {
                                        remove(position);
                                        dialog1.dismiss();
                                        BasicData.getInstance(context).createToast(context.getResources()
                                                .getString(R.string.removed));
                                    });
                        }).show();
            }
        });
    }


    private void remove(int position) {
        sections.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, sections.size());
    }

    @Override
    public int getItemCount() {
        return sections.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView titleTV;
        TextView instructorTV;
        ImageView infoIV;
        LinearLayout progressLL;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTV = itemView.findViewById(R.id.titleTV);
            instructorTV = itemView.findViewById(R.id.instructorTV);
            infoIV = itemView.findViewById(R.id.infoIV);
            progressLL = itemView.findViewById(R.id.progressLL);
        }
    }
}
