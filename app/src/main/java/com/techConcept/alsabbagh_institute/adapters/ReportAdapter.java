package com.techConcept.alsabbagh_institute.adapters;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.techConcept.alsabbagh_institute.BasicData;
import com.techConcept.alsabbagh_institute.models.Report;
import com.techConcept.alsabbagh_institute.server.APIs;
import com.techConcept.alsabbagh_institute.server.ServerManager;

import java.util.ArrayList;

import techContept.theinstitute.R;

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ViewHolder> {

    Context context;
    ArrayList<Report> reports;

    public ReportAdapter(Context context, ArrayList<Report> reports) {
        this.context = context;
        this.reports = reports;
    }

    @NonNull
    @Override
    public ReportAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ReportAdapter.ViewHolder(
                LayoutInflater.from(context)
                        .inflate(R.layout.report_item,
                                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ReportAdapter.ViewHolder holder, int position) {
        Report report = reports.get(position);
        holder.body.setText(report.getBody());
        holder.date.setText(report.getDate());
        holder.deleteIV.setOnClickListener(v -> {
            new AlertDialog.Builder(context)
                    .setMessage(context.getResources().getString(R.string.sure_remove))
                    .setNegativeButton(context.getResources().getString(R.string.no), null)
                    .setPositiveButton(context.getResources().getString(R.string.yes), (dialog, which) -> {
                        String token = BasicData.getInstance(context).getTOKEN();
                        ProgressDialog dialog1 = new ProgressDialog(context);
                        dialog1.setMessage(context.getResources().getString(R.string.removing));
                        dialog1.setCancelable(false);
                        dialog1.show();
                        ServerManager.delete(context, APIs.deleteReport + report.getId(), token, dialog1,
                                result -> {
                                    remove(position);
                                    dialog1.dismiss();
                                    BasicData.getInstance(context).createToast(context.getResources()
                                            .getString(R.string.removed));
                                });
                    }).show();
        });
    }

    private void remove(int position) {
        reports.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, reports.size());
    }

    @Override
    public int getItemCount() {
        return reports.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView date;
        TextView body;
        ImageView deleteIV;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            date = itemView.findViewById(R.id.dateTV);
            body = itemView.findViewById(R.id.bodyTV);
            deleteIV = itemView.findViewById(R.id.deleteIV);
        }
    }
}
