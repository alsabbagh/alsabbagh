package com.techConcept.alsabbagh_institute.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.techConcept.alsabbagh_institute.models.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import techContept.theinstitute.R;

public class TestAdapter extends RecyclerView.Adapter<TestAdapter.ViewHolder> {

    Context context;
    ArrayList<Test> tests;

    public TestAdapter(Context context, ArrayList<Test> tests) {
        this.context = context;
        this.tests = tests;
    }

    @NonNull
    @Override
    public TestAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TestAdapter.ViewHolder(
                LayoutInflater.from(context)
                        .inflate(R.layout.test_item,
                                parent, false));
    }

    @SuppressLint({"SetTextI18n", "SimpleDateFormat"})
    @Override
    public void onBindViewHolder(@NonNull TestAdapter.ViewHolder holder, int position) {
        Test test = tests.get(position);
        holder.titleTV.setText("الاختبار: " + test.getTitle());
        holder.courseTV.setText("المادة: " + test.getCourse());
        if (test.getMark() != -1) {
            holder.markTV.setText(context.getResources().getString(R.string.mark) + " " + test.getMark() + " من " + test.getFullMark());
        } else {
            holder.markTV.setText(context.getResources().getString(R.string.not_done));
        }
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            Date date = dateFormat.parse(test.getDate());
            DateFormat dateFormat_ = new SimpleDateFormat("dd/MM/yyyy");
            assert date != null;
            String date_ = dateFormat_.format(date);
            holder.dateTV.setText(date_);
        } catch (Exception e) {
            System.out.println("ssss" + e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return tests.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView titleTV;
        TextView dateTV;
        TextView markTV;
        TextView courseTV;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            titleTV = itemView.findViewById(R.id.titleTV);
            courseTV = itemView.findViewById(R.id.courseTV);
            dateTV = itemView.findViewById(R.id.dateTV);
            markTV = itemView.findViewById(R.id.markTV);
        }
    }
}
