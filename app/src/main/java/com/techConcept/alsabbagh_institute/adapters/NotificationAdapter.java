package com.techConcept.alsabbagh_institute.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.techConcept.alsabbagh_institute.CourseDetailsActivity;
import com.techConcept.alsabbagh_institute.models.Notification;

import java.util.ArrayList;

import techContept.theinstitute.R;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    Context context;
    ArrayList<Notification> notifications;

    public NotificationAdapter(Context context, ArrayList<Notification> notifications) {
        this.context = context;
        this.notifications = notifications;
    }

    @NonNull
    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NotificationAdapter.ViewHolder(
                LayoutInflater.from(context)
                        .inflate(R.layout.notification_item,
                                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.ViewHolder holder, int position) {
        Notification notification = notifications.get(position);
        holder.titleTV.setText(notification.getTitle());
        holder.bodyTV.setText(notification.getBody());
        if (notification.getSectionId() != null || !notification.getSectionId().isEmpty()) {
            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(context, CourseDetailsActivity.class);
                intent.putExtra("id", notification.getSectionId());
                context.startActivity(intent);
            });
        }
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleTV;
        TextView bodyTV;
        CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTV = itemView.findViewById(R.id.titleTV);
            bodyTV = itemView.findViewById(R.id.bodyTV);
            cardView = itemView.findViewById(R.id.card);
        }
    }
}
