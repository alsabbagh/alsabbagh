package com.techConcept.alsabbagh_institute;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.techConcept.alsabbagh_institute.server.APIs;
import com.techConcept.alsabbagh_institute.server.ServerManager;

import org.json.JSONException;
import org.json.JSONObject;
import techContept.theinstitute.R;

public class LoginActivity extends AppCompatActivity {

    private EditText usernameET;
    private EditText passwordET;
    private Button loginBTN;
    public static Activity LoginActivityPage;
    //    private TextView forgetPasswordTV;
    @SuppressLint("StaticFieldLeak")
    public static LinearLayout progressLL;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        usernameET = findViewById(R.id.usernameET);
        passwordET = findViewById(R.id.passwordET);
        loginBTN = findViewById(R.id.loginBTN);
        LoginActivityPage = this;
        TextView noAccount = findViewById(R.id.no_account);
//        forgetPasswordTV = findViewById(R.id.forgetPasswordTV);
        TextView loginTV = findViewById(R.id.loginTV);
        progressLL = findViewById(R.id.progressLL);
        BasicData basicData = BasicData.getInstance(this);
        basicData.setGradientColor(loginTV);
//        basicData.setGradientColor(forgetPasswordTV);

        loginBTN.setOnClickListener(v -> {
            if (checkData()) {
                try {
                    progressLL.setVisibility(View.VISIBLE);
                    JSONObject jsonBody = new JSONObject();
                    jsonBody.put("username", usernameET.getText().toString().trim());
                    jsonBody.put("password", passwordET.getText().toString().trim());
                    final String requestBody = jsonBody.toString();
                    ServerManager.post(this, APIs.login, requestBody, "", progressLL, result -> {
                        JSONObject user;
                        try {
                            JSONObject resultJSON = new JSONObject(result);
                            user = new JSONObject(resultJSON.getString("data"));
                            Log.e("User", String.valueOf(user));
                            basicData.setUserData(user);
                            basicData.setTOKEN(resultJSON.getString("token"));
                            progressLL.setVisibility(View.GONE);
                            finish();
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        } catch (JSONException e) {
                            Log.e("loginEXJSON", e.getMessage());
                        }
                    });
                } catch (Exception e) {
                    Log.e("loginEX", e.getMessage());
                }
            } else {
                Snackbar.make(this.loginBTN, R.string.full_info, Snackbar.LENGTH_LONG)
                        .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                        .setBackgroundTint(getResources().getColor(R.color.dark_blue))
                        .show();
            }
        });

        noAccount.setOnClickListener(v -> {
            startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
        });

//        forgetPasswordTV.setOnClickListener(v ->
//                startActivity(new Intent(LoginActivity.this, ForgetPasswordActivity.class)));
    }

    private boolean checkData() {
        return !usernameET.getText().toString().trim().isEmpty() &&
                !passwordET.getText().toString().trim().isEmpty();
    }
}