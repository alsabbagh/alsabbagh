package com.techConcept.alsabbagh_institute;

import static com.techConcept.alsabbagh_institute.LoginActivity.LoginActivityPage;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.techConcept.alsabbagh_institute.models.Class;
import com.techConcept.alsabbagh_institute.server.APIs;
import com.techConcept.alsabbagh_institute.server.ServerManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import techContept.theinstitute.R;

public class RegisterActivity extends AppCompatActivity {

    private EditText usernameET;
    private EditText passwordET;
    private EditText fullNameET;
    private EditText mobilePhoneET;
    private EditText fatherPhoneET;
    private EditText motherPhoneET;
    private EditText phoneET;
    private ArrayList<Class> classes;
    private BasicData basicData;
    private Spinner classesSpinner;
    private Button registerBTN;
    private TextView loginTV;
    private LinearLayout progressLL;
    private RadioGroup gender;
    Button doneBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        usernameET = findViewById(R.id.usernameET);
        passwordET = findViewById(R.id.passwordET);
        fullNameET = findViewById(R.id.fullNameET);
        mobilePhoneET = findViewById(R.id.mobilePhoneET);
        fatherPhoneET = findViewById(R.id.fatherPhoneET);
        motherPhoneET = findViewById(R.id.motherPhoneET);
        phoneET = findViewById(R.id.phoneET);
        classesSpinner = findViewById(R.id.classesSpinner);
        registerBTN = findViewById(R.id.registerBTN);
        progressLL = findViewById(R.id.progressLL);
        loginTV = findViewById(R.id.loginTV);
        gender = findViewById(R.id.genderGroup);

        basicData = BasicData.getInstance(this);

        getClasses();

        classesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                ((TextView) parent.getSelectedView()).setTextColor(getResources().getColor(R.color.dark_blue));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        registerBTN.setOnClickListener(v -> {
            if (checkData()) {
                try {
                    Class selectedItem = (Class) classesSpinner.getSelectedItem();

                    RadioButton _gender
                            = gender
                            .findViewById(gender.getCheckedRadioButtonId());

                    String gender_ = _gender.getText().toString();

                    progressLL.setVisibility(View.VISIBLE);
                    JSONObject jsonBody = new JSONObject();
                    jsonBody.put("username", usernameET.getText().toString().trim());
                    jsonBody.put("password", passwordET.getText().toString().trim());
                    jsonBody.put("name", fullNameET.getText().toString().trim());
                    jsonBody.put("mobilePhone", mobilePhoneET.getText().toString().trim());
                    jsonBody.put("fatherPhone", fatherPhoneET.getText().toString().trim());
                    jsonBody.put("motherPhone", motherPhoneET.getText().toString().trim());
                    jsonBody.put("phone", phoneET.getText().toString().trim());
                    jsonBody.put("classId", selectedItem.getId());
                    jsonBody.put("gender", gender_.equals("ذكر") ? "male" : "female");
                    final String requestBody = jsonBody.toString();
                    ServerManager.post(this, APIs.register, requestBody, "", progressLL, result -> {
                        JSONObject user;
                        try {
                            showTermsDialog();

                            JSONObject resultJSON = new JSONObject(result);
                            user = new JSONObject(resultJSON.getString("data"));
                            basicData.setUserData(user);
                            basicData.setTOKEN(resultJSON.getString("token"));
                        } catch (JSONException e) {
                            Log.e("loginEXJSON", e.getMessage());
                        }
                    });
                } catch (Exception e) {
                    Log.e("loginEX", e.getMessage());
                }
            } else {
                Snackbar.make(this.registerBTN, R.string.full_info, Snackbar.LENGTH_LONG)
                        .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                        .setBackgroundTint(getResources().getColor(R.color.dark_blue))
                        .show();
            }
        });

        loginTV.setOnClickListener(v -> {
            finish();
            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        });

    }

    private void getClasses() {
        classes = new ArrayList<>();

        ServerManager.get(this, APIs.classes,
                basicData.getTOKEN(), null, result -> {
                    try {
                        JSONArray data = new JSONArray(new JSONObject(result).getString("data"));

                        for (int i = 0; i < data.length(); i++) {
                            classes.add(new Class(data.getJSONObject(i).getString("id"), data.getJSONObject(i).getString("title")));
                        }

                        if (classes.size() != 0) {
                            classesSpinner.setVisibility(View.VISIBLE);
                            ArrayAdapter classesAdapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, classes);

                            classesSpinner.setAdapter(classesAdapter);
                            classesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                    Class _class = (Class) parent.getSelectedItem();

                                    Log.e("EXE", _class.getId());
                                    Log.e("EXE", _class.getTitle());
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });
                        } else {
                            basicData.createToast(getResources().getString(R.string.no_tests_yet));
                        }
                    } catch (Exception e) {
                        Log.e("EXE", e.getMessage());
                    }
                });
    }

    private boolean checkData() {
        return !usernameET.getText().toString().trim().isEmpty() &&
                !passwordET.getText().toString().trim().isEmpty() &&
                !fullNameET.getText().toString().trim().isEmpty() &&
                !fatherPhoneET.getText().toString().trim().isEmpty() &&
                !mobilePhoneET.getText().toString().trim().isEmpty() &&
                !classesSpinner.getSelectedItem().toString().isEmpty();
    }

    private void showTermsDialog() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.terms_conditions_dialog);
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();

        doneBTN = dialog.findViewById(R.id.doneBTN);

        doneBTN.setOnClickListener(v1 -> {
                    progressLL.setVisibility(View.GONE);
                    LoginActivityPage.finish();
                    finish();
                    startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                    dialog.dismiss();
                }
        );

        dialog.show();
    }

}