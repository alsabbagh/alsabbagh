package com.techConcept.alsabbagh_institute.server;

public interface APIs {

    String publicUsers = "public/users/";
    String login = publicUsers + "login";
    String register = publicUsers + "register";

    String privateUsers = "private/users/";
    String updatePassword = privateUsers + "updatePassword";
    String sections = privateUsers + "sections";
    String removeSection = privateUsers + "sections/";
    String program = privateUsers + "program";

    String activities = "private/tests/";
    String getTests = activities + "user";

    String reports = "private/reports/";
    String getReports = reports + "getById";
    String add = reports + "add";
    String deleteReport = reports + "delete/";

    String notifications = "private/notifications/";
    String unread = notifications + "unread";
    String getAll = notifications + "all";
    String update = notifications + "update";

    String classes = "public/classes";

    String allSections = "private/sections";

    String stories = "private/stories";
    String userStory = stories + "/user-story";

    String version = "public/server";

}
