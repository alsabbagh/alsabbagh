package com.techConcept.alsabbagh_institute.server;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.techConcept.alsabbagh_institute.BasicData;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import techContept.theinstitute.R;

public class ServerManager {

    //    public final static String ROOT = "http://192.168.0.227:4000";
    public final static String ROOT = "https://backend.s.alsabbagh.atconcept.tech";
    final static String BASE_URL = ROOT + "/api/";
    @SuppressLint("StaticFieldLeak")

    static BasicData basicData;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void post(Context context, String partURL, String requestBody,
                            String token, LinearLayout progress,
                            final VolleyCallback volleyCallback) {
        RequestQueue mRequestQueue = Volley.newRequestQueue(context);
        String requestURL = BASE_URL + partURL;
        basicData = BasicData.getInstance(context);
        StringRequest mStringRequest = new StringRequest(Request.Method.POST,
                requestURL, volleyCallback::onSuccess,
                error -> {
                    try {
                        progress.setVisibility(View.GONE);
                        JSONObject data = new JSONObject(new String(error.networkResponse.data,
                                StandardCharsets.UTF_8));
                        basicData.createToast(data.optString("data"));
                    } catch (Exception e) {
                        basicData.createToast(context.getResources().getString(R.string.check_internet));
                        e.printStackTrace();
                    }
                }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + token);
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = new String(response.data, StandardCharsets.UTF_8);
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
        };
        mRequestQueue.add(mStringRequest);
    }

    public static void get(Context context, String partURL, String token, LinearLayout progress,
                           final VolleyCallback volleyCallback) {
        RequestQueue mRequestQueue = Volley.newRequestQueue(context);
        String requestURL = BASE_URL + partURL;
        basicData = BasicData.getInstance(context);
        StringRequest mStringRequest = new StringRequest(Request.Method.GET,
                requestURL, volleyCallback::onSuccess,
                error -> {
                    try {
                        if (progress != null) {
                            progress.setVisibility(View.GONE);
                        }
                        JSONObject data = new JSONObject(new String(error.networkResponse.data,
                                StandardCharsets.UTF_8));
                        basicData.createToast(data.optString("data"));
                    } catch (Exception e) {
                        basicData.createToast(context.getResources().getString(R.string.check_internet));
                        e.printStackTrace();
                    }
                }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + token);
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = new String(response.data, StandardCharsets.UTF_8);
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
        };
        mRequestQueue.add(mStringRequest);
    }

    public static void put(Context context, String partURL, String requestBody,
                           String token, LinearLayout progress,
                           final VolleyCallback volleyCallback) {
        RequestQueue mRequestQueue = Volley.newRequestQueue(context);
        String requestURL = BASE_URL + partURL;
        basicData = BasicData.getInstance(context);
        StringRequest mStringRequest = new StringRequest(Request.Method.PUT,
                requestURL, volleyCallback::onSuccess,
                error -> {
                    try {
                        progress.setVisibility(View.GONE);
                        JSONObject data = new JSONObject(new String(error.networkResponse.data,
                                StandardCharsets.UTF_8));
                        basicData.createToast(data.optString("data"));
                    } catch (Exception e) {
                        basicData.createToast(context.getResources().getString(R.string.check_internet));
                    }
                }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + token);
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = new String(response.data, StandardCharsets.UTF_8);
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
        };
        mRequestQueue.add(mStringRequest);
    }

    public static void delete(Context context, String partURL, String token, ProgressDialog dialog,
                              final VolleyCallback volleyCallback) {
        RequestQueue mRequestQueue = Volley.newRequestQueue(context);
        String requestURL = BASE_URL + partURL;
        StringRequest mStringRequest = new StringRequest(Request.Method.DELETE,
                requestURL, volleyCallback::onSuccess,
                error -> {
                    try {
                        JSONObject data = new JSONObject(new String(error.networkResponse.data,
                                StandardCharsets.UTF_8));
                        dialog.dismiss();
                        basicData = BasicData.getInstance(context);
                        basicData.createToast(data.optString("data"));
                    } catch (Exception e) {
                        dialog.dismiss();
                        basicData.createToast(context.getResources().getString(R.string.check_internet));
                        e.printStackTrace();
                    }
                }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + token);
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = new String(response.data, StandardCharsets.UTF_8);
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
        };
        mRequestQueue.add(mStringRequest);
    }
}
