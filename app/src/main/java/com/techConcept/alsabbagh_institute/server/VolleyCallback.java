package com.techConcept.alsabbagh_institute.server;

public interface VolleyCallback {
    void onSuccess(String result);
}
