package com.techConcept.alsabbagh_institute;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.techConcept.alsabbagh_institute.server.APIs;
import com.techConcept.alsabbagh_institute.server.ServerManager;

import org.json.JSONObject;
import techContept.theinstitute.R;

public class MainActivity extends AppCompatActivity {

    private static BottomNavigationView navigationView;
    public static BadgeDrawable badge;
    private boolean doubleTab = false;
    private BasicData basicData;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        onNewIntent(getIntent());
        navigationView = findViewById(R.id.navBar);
        basicData = BasicData.getInstance(this);

        ServerManager.get(this, APIs.unread, basicData.getTOKEN(), linearLayout, result -> {
            try {
                JSONObject data = new JSONObject(result);
                int notifications = (data.getInt("data"));
                if (notifications != 0) {
                    badge = MainActivity.navigationView.getOrCreateBadge(R.id.notifications);
                    badge.setNumber(notifications);
                }
            } catch (Exception e) {
                Log.e("Exception_EX", e.getMessage());
            }
        });
        getSupportFragmentManager().beginTransaction().replace(R.id.homeActivity,
                new HomeFragment()).commit();
        navigationView.setOnNavigationItemSelectedListener(item -> {
            Fragment fragment;
            if (item.getItemId() == R.id.home) {
                fragment = new HomeFragment();
            } else if (item.getItemId() == R.id.notifications) {
                navigationView.removeBadge(R.id.notifications);
                fragment = new NotificationsFragment();
            } else if (item.getItemId() == R.id.profile) {
                fragment = new ProfileFragment();
            } else if (item.getItemId() == R.id.sections) {
                fragment = new SectionsFragment();
            } else {
                fragment = new ReportsFragment();
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.homeActivity,
                    fragment).commit();
            return true;
        });
        String type1 = getIntent().getStringExtra("type1");
        if (type1 != null && type1.equals("notifications")) {
            navigationView.removeBadge(R.id.notifications);
            navigationView.setSelectedItemId(R.id.notifications);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleTab) {
            super.onBackPressed();
        } else {
            basicData.createToast(getResources().getString(R.string.tab_double));
            doubleTab = true;
            Handler handler = new Handler();
            handler.postDelayed(() -> doubleTab = false, 2000);
        }
    }

}